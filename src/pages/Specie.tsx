import { useEffect, useState } from 'react';
import { Params, useParams } from 'react-router-dom';
import { API_VEHICLES } from '../helpers/constants/swapiEndpoints';
import { ISpecie } from '../helpers/interfaces/ISpecie';
import { TSpecies } from '../helpers/types/TSpecies';

const Specie = ({ species }: TSpecies): JSX.Element => {
    const [isLoad, setIsLoad] = useState<boolean>(false);
    const [specieName, setSpecieName] = useState<string | undefined>('');
    const [userName, setUserName] = useState<string | undefined>('');
    const [specieVehicles, setSpecieVehicles] = useState<Array<object> | undefined>();

    const params: Params<string> = useParams();

    useEffect(() => {
        setIsLoad(false);

        const specie: ISpecie | undefined = species.find(({ name }) => name.toLowerCase() === params.specieName);
        const userUrl: string | undefined = specie?.people ? specie.people[0] : undefined;

        setSpecieName(specie?.name);

        let specieVehiclesUrl: string = getSpecieVehiclesUrl(specie);

        if (userUrl && specieVehiclesUrl) {
            getUserAndSpecieVehiclesData(userUrl, specieVehiclesUrl);
        } else if (userUrl) {
            getUserData(userUrl);
        }
    }, [params]);

    const getSpecieVehiclesUrl = (specie: ISpecie | undefined) => {
        switch (specie?.name) {
            case 'Human':
                return API_VEHICLES;
            case 'Droid':
                return API_VEHICLES + '?search=droid';
            default:
                return '';
        }
    };

    const getUserAndSpecieVehiclesData = (userUrl: string | undefined, specieVehiclesUrl: string) => {
        const data: Promise<any> = Promise.all([]);
    };

    const getUserData = (userUrl: string | undefined) => {};

    return <></>;
};

export default Specie;
